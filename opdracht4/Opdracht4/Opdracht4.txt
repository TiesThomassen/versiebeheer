Studentnummer:
Naam:

Hoe open je de Terminal in Visual Studio Code?
	# CTRL + `

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status

Wat is het commando van een multiline commit message?
	# git commit -m "" -m ""

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	# 9

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	# git reset --soft HEAD~1
